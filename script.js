//1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

//Рішення
function ex1() {
    const allArray = clients1.concat(clients2);
    const trueArray = allArray.filter((value, index, element) => element.indexOf(value) === index);
    console.log(trueArray);
}
ex1()



//2
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
];

//Рішення
function ex2() {
    const trueArray = characters.map(({name,lastName,age})=>({name,lastName,age}))
    console.log(trueArray)
}
ex2()



//3
const user1 = {
    name: "John",
    years: 30
};

//Рішення
function ex3(){
    const { name: імя, years: вік, isAdmin = false } = user1;

    console.log(`${user1.name} ${user1.years} ${isAdmin}`)
}
ex3()



//4
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
}
  
const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}
  
const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

//Рішення
function ex4() {
    let trueObject = {...satoshi2018, ...satoshi2019, ...satoshi2020}
    console.log(trueObject)
}
ex4()



//5
const books = [
    {
        name: 'Harry Potter',
        author: 'J.K. Rowling'
    }, 

    {
        name: 'Lord of the rings',
        author: 'J.R.R. Tolkien'
    },

    {
        name: 'The witcher',
        author: 'Andrzej Sapkowski'
    }
];
  
const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

//Рішення
function ex5(){
    let trueArray = [...books, bookToAdd]
    console.log(trueArray)
}
ex5()



//6
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

//Рішення
function ex6(){
    let newEmployee = {...employee, age: 'old', salary: 'big'}
    console.log(newEmployee)
}
ex6()



//7
const array = ['value', () => 'showValue'];

const [value,showValue] = array // Допишіть код тут


alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'